#include<iostream>
#include<fstream>
#include<vector>
#include<Eigen/SVD>
#include<string>
#include<sstream>
#include"include/CPlanePoints.h"
#include<exception>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include "include/PCLHelper.h"

using namespace std;
using namespace Eigen;
using namespace pcl;

const int MAXPLANES=10000;

char* seg_plane_file_name;
char* laser_point_name;

bool myfunction(CPlanePoints pp1, CPlanePoints pp2)
{
    return pp1.m_points.size()>pp2.m_points.size();
}

vector<CPlanePoints> ReadPlanes(char* filename)
{
    ifstream planefile(filename);
    vector<CPlanePoints> pps;
    string line;
    while(getline(planefile,line))
    {
        CPlanePoints pp;
        istringstream is(line);
        int planeId;
        is>>planeId;
        pp.m_planeId=planeId;
        vector<double> dCoeffs(4);
        is>>dCoeffs[0]>>dCoeffs[1]>>dCoeffs[2]>>dCoeffs[3];

        pp.m_dCoeffs=dCoeffs;
        vector<double> coeffs(4);
        is>>coeffs[0]>>coeffs[1]>>coeffs[2]>>coeffs[3];
        char isValid;
        is>>isValid;
        pp.m_coeffs=coeffs;
        pps.push_back(pp);
    }
    return pps;
}

void ReadPoints(char* filename, vector<CPlanePoints>& pps)
{
    ifstream pointfile(filename);
    string line;
    while(getline(pointfile,line))
    {
        istringstream is(line);
        int planeId;
        is>>planeId;
        vector<double> pt(3);
        is>>pt[0]>>pt[1]>>pt[2];
        if(!(pt[0]==0 && pt[1]==0 && pt[2]==0))
        {

            pps[planeId].m_points.push_back(pt);
        }
    }
}

vector<int> FindLargestPlanes(vector<CPlanePoints> pps, int planeNum)
{
    /*int maxPointNum=0,maxPlaneId=-1;;
    for(int i=0;i<pps.size();i++)
    {
        int ptNum=pps[i].m_points.size();
        if(ptNum>=maxPointNum)
        {
            maxPointNum=ptNum;
            maxPlaneId=i;
        }
    }*/
    vector<int> ids;
    sort(pps.begin(),pps.end(),myfunction);
    for(int i=0;i<min((int)planeNum,(int)pps.size());i++)
    {
        ids.push_back(pps[i].m_planeId);
    }
    return ids;
}

void FitLargestPlane(CPlanePoints pps,vector<double>& coeffs,vector<double>& pcoeffs)
{
    vector<vector<double> > pts=pps.m_points;
   // vector<double> coeffs;
    int flag=FitPlane(pts,coeffs);
    if(flag==0)
    {
        cout<<coeffs[0]<<" "<<coeffs[1]<<" "<<coeffs[2]<<" "<<coeffs[3]<<endl;
    }
    /*coeffs[0]/=coeffs[3];
    coeffs[1]/=coeffs[3];
    coeffs[2]/=coeffs[3];
    coeffs[3]/=coeffs[3];*/
    pcoeffs=pps.m_coeffs;
    pps.DisplayCoeffs();
}

void Normalize(vector<double>& n)
{
    double length=sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
    n[0]/=length;
    n[1]/=length;
    n[2]/=length;
}

MatrixXd RotationEstimation(vector<double> nr, vector<double> nc)
{
    Normalize(nr);
    Normalize(nc);
    MatrixXd nr_mat(3,1), nc_mat(3,1);
    for(int i=0;i<3;i++)
    {
        nr_mat(i,0)=nr[i];
        nc_mat(i,0)=nc[i];
    }
    cout<<nr_mat<<endl;
    cout<<nc_mat<<endl;
    JacobiSVD<MatrixXd> svd(nc_mat*nr_mat.transpose(),ComputeThinU | ComputeThinV);
    MatrixXd U=svd.matrixU();
    MatrixXd V=svd.matrixV();
    MatrixXd res=V*U.transpose();
    return res;
}

void NormalEstimation()
{

}

void GlobalRegistration()
{

}

void ParseInput(int argc, char** argv)
{
    if(argc<3)
    {
        throw "not enough arguments. Usage: seg_plane_file_name, laser_point_name";
    }
    seg_plane_file_name=argv[1];
    laser_point_name=argv[2];
}

void ParseInput2(int argc, char** argv)
{
    if(argc<2)
    {
        throw "not enough arguments. Usage: laser_point_name";
    }
    laser_point_name=argv[1];
}

void ReadPoints2(char* filename, vector<vector<double> >& pts)
{
    ifstream pointfile(filename);
    string line;
    pts.clear();
    while(getline(pointfile,line))
    {
        istringstream is(line);
        vector<double> pt(6);
        is>>pt[0]>>pt[1]>>pt[2]>>pt[3]>>pt[4]>>pt[5];
            pts.push_back(pt);
    }
}

int main(int argc, char** argv)
{
    try
    {
        ParseInput2(argc,argv);
    }
    catch(const char* e)
    {
        cout<<e<<endl;
        return -1;
    }
    //vector<CPlanePoints> pps=ReadPlanes(seg_plane_file_name);
    vector<vector<double> > pts;
    ReadPoints2(laser_point_name,pts);
    vector<vector<double> > pts_out;
    vector<int> ind;
    RemoveOutliers(pts,pts_out,ind);
    cout<<pts.size()<<" "<<pts_out.size()<<" "<<ind.size()<<endl;
    ofstream newPts("laser_points_new.txt");
    int j=0;
    for(int i=0;i<pts.size();i++)
    {
        if(i==ind[j])
        {
          newPts<<"0 0 0 0 0 0"<<endl;
         j++;
        }
        else
        {
            newPts<<pts[i][0]<<" "<<pts[i][1]<<" "<<pts[i][2]<<" "<<pts[i][3]<<" "<<pts[i][4]<<" "<<pts[i][5]<<endl;
        }
    }
    newPts.close();
    /*int planeNum=3;
    vector<int> ids=FindLargestPlanes(pps,planeNum);
    cout<<pps.size()<<endl;
    for(int i=0;i<planeNum;i++)
        cout<<ids[i]<<" ";
    cout<<endl;
    //cout<<pps[maxPlaneId].m_points.size()<<endl;
    ofstream fcoeffs("point_coeffs.txt"),fpcoeffs("plane_coeffs.txt");
    for(int i=0;i<planeNum;i++)
    {
        vector<double> coeffs,pcoeffs;
        FitLargestPlane(pps[ids[i]],coeffs,pcoeffs);
        fcoeffs<<coeffs[0]<<" "<<coeffs[1]<<" "<<coeffs[2]<<" "<<coeffs[3]<<endl;
        fpcoeffs<<pcoeffs[0]<<" "<<pcoeffs[1]<<" "<<pcoeffs[2]<<" "<<pcoeffs[3]<<endl;
        MatrixXd normal=RotationEstimation(coeffs,pcoeffs);
        cout<<"normal: "<<normal<<endl;
    }
*/

    return 0;
}
