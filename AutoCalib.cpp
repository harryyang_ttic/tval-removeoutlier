#include<iostream>
#include<fstream>
#include<vector>
#include<Eigen/SVD>
#include<string>
#include<sstream>
#include"include/CPlanePoints.h"
#include<exception>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include "include/PCLHelper.h"

using namespace std;
using namespace Eigen;
using namespace pcl;

const int MAXPLANES=10000;

char* seg_plane_file_name;
char* laser_point_name, *laser_out_name;



void ParseInput(int argc, char** argv)
{
    if(argc<2)
    {
        throw "not enough arguments. Usage: point_name, point_name_out";
    }
    laser_point_name=argv[1];
    laser_out_name=argv[2];
}

void ReadPoints(char* filename, vector<vector<double> >& pts)
{
    ifstream pointfile(filename);
    string line;
    pts.clear();
    while(getline(pointfile,line))
    {
        istringstream is(line);
        vector<double> pt(6);
        is>>pt[0]>>pt[1]>>pt[2]>>pt[3]>>pt[4]>>pt[5];
            pts.push_back(pt);
    }
}

int main(int argc, char** argv)
{
    try
    {
        ParseInput(argc,argv);
    }
    catch(const char* e)
    {
        cout<<e<<endl;
        return -1;
    }
    //vector<CPlanePoints> pps=ReadPlanes(seg_plane_file_name);
    vector<vector<double> > pts;
    ReadPoints(laser_point_name,pts);
    vector<vector<double> > pts_out;
    vector<int> ind;
    RemoveOutliers(pts,pts_out,ind);
    cout<<"point_total point_after_outlier outlier: "<<pts.size()<<" "<<pts_out.size()<<" "<<ind.size()<<endl;
    ofstream newPts(laser_out_name);
    int j=0;
    for(int i=0;i<pts.size();i++)
    {
        if(i==ind[j])
        {
          newPts<<"0 0 0 0 0 0"<<endl;
          j++;
        }
        else
        {
            newPts<<pts[i][0]<<" "<<pts[i][1]<<" "<<pts[i][2]<<" "<<pts[i][3]<<" "<<pts[i][4]<<" "<<pts[i][5]<<endl;
        }
    }
    newPts.close();
    return 0;
}
