#ifndef CPLANEPOINTS_H
#define CPLANEPOINTS_H

#include<vector>
class CPlanePoints
{
    public:
    std::vector<double> m_coeffs;
    std::vector<double> m_dCoeffs;
    std::vector<std::vector<double> > m_points;
    int m_planeId;
      void DisplayCoeffs();

        CPlanePoints();

        virtual ~CPlanePoints();
    protected:
    private:
};

#endif // CPLANEPOINTS_H
