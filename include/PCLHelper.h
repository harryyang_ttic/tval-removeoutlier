#ifndef PCLHELPER_H_INCLUDED
#define PCLHELPER_H_INCLUDED

#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/extract_indices.h>


#include<vector>

inline int FitPlane(std::vector<std::vector<double> > pts, std::vector<double>& coeffs)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  cloud->width=pts.size();
  cloud->height=1;
  cloud->points.resize(cloud->width*cloud->height);

  for(int i=0;i<cloud->points.size();i++)
  {
    cloud->points[i].x=pts[i][0];
    cloud->points[i].y=pts[i][1];
    cloud->points[i].z=pts[i][2];
  }

  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloud);
  seg.segment (*inliers, *coefficients);

  if (inliers->indices.size () == 0)
  {
    PCL_ERROR ("Could not estimate a planar model for the given dataset.");
    return (-1);
  }

    coeffs.resize(4);
    for(int i=0;i<4;i++)
        coeffs[i]=coefficients->values[i];
 return 0;
}

inline void RemoveOutliers(std::vector<std::vector<double> > pts, std::vector<std::vector<double> >& pts_out, std::vector<int >& indices)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  cloud->width=pts.size();
  cloud->height=1;
  cloud->points.resize(cloud->width*cloud->height);

  for(int i=0;i<cloud->points.size();i++)
  {
    cloud->points[i].x=pts[i][0];
    cloud->points[i].y=pts[i][1];
    cloud->points[i].z=pts[i][2];
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor(true);
  sor.setInputCloud (cloud);
  sor.setMeanK (50);
  sor.setStddevMulThresh (1.0);
  sor.filter (*cloud_filtered);

  pts_out.resize(cloud_filtered->points.size());
  for(int i=0;i<cloud_filtered->points.size();i++)
  {
        pts_out[i].resize(3);
        pts_out[i][0]=cloud_filtered->points[i].x;
        pts_out[i][1]=cloud_filtered->points[i].y;
        pts_out[i][2]=cloud_filtered->points[i].z;
  }
  pcl::IndicesConstPtr ind=sor.getRemovedIndices();
  pcl::PointIndices::Ptr removed_indices (new pcl::PointIndices());
  removed_indices->indices = *ind;
  indices.resize(removed_indices->indices.size());
  for(int i=0;i<removed_indices->indices.size();i++)
  {
    indices[i]=removed_indices->indices[i];
  }

}
#endif // PCLHELPER_H_INCLUDED
