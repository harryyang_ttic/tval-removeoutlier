#include "../include/CPlanePoints.h"
#include<iostream>
using namespace std;

CPlanePoints::CPlanePoints()
{
    //ctor
    m_planeId=-1;
    m_coeffs.resize(4);
    m_dCoeffs.resize(4);
}

CPlanePoints::~CPlanePoints()
{
    //dtor
}


void CPlanePoints::DisplayCoeffs()
{
    for(int i=0;i<4;i++)
    {
        cout<<m_coeffs[i]<<" ";
    }
    cout<<endl;
}
